import os
import json
import time
import requests
from copy import deepcopy

from flask import request, Response
from flask_restx import Resource, Namespace

from cache_decorator_redis_ubit import NoCacheDecorator, CacheDecorator, CacheDecorator12h

from iiif_text2image_emanu.iiif_text2image import IIIFText2Layout

CACHE = CacheDecorator()

TRANSCRIBUS_COLLECTION = os.getenv("TRANSCRIBUS_COLLECTION")
TRANSCRIBUS_LOGIN = os.getenv("TRANSCRIBUS_LOGIN")
TRANSCRIBUS_PWD = os.getenv("TRANSCRIBUS_PWD")
LOGGING_LEVEL = "INFO"

ns_text2iiif = Namespace('text2iiif', description="build text overlay for fulltext using transkribus apis")
parser = ns_text2iiif.parser()
parser.add_argument('only_new_export', type=bool, help='Start a new export to cache from Transkribus data '
                                                       '(e.g. if it has been corrected manually)')
parser.add_argument('new_processing', type=bool, help='Process the Vlmid again (no iiif upload) '
                                                      'but layout and extract emanus text again')

# TODO nur Cache èberprüfung, aonssten wird neu prozessiert -> problematisch mit 12h Cache
# TODO überprüfen wann Cache erzeugt wurde mit letzten Veränderungsdatum auf Seite

@ns_text2iiif.route('/altoxml/<vlmid>/<fileid>', methods=['GET'])
@ns_text2iiif.route('/altoxml/<vlmid>/<fileid>/', methods=['GET'])
class EnrichedIIIFManifest(Resource):

    @classmethod
    def get(cls, vlmid=None, fileid=None):
        """
        return Alto XML file from cache with fulltext and coordinates
        which have been processed and stored in cache beforehand
        :param vlmid: vlmid for an object that has alto xml pages
        :param fileid: fileid for an vlm object with transcriptions
        :return: Alto XML file for page from Cache
        """
        r = CACHE.get_keyvalue_cache(key=f"{vlmid}_{fileid}")
        resp = Response(r)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/xml"
        return resp


@ns_text2iiif.route('/addalto/<vlmid>/manifest.json', methods=['GET'])
@ns_text2iiif.route('/addalto/<vlmid>/manifest.json', methods=['GET'])
class EnrichedIIIFManifest(Resource):

    @classmethod
    @ns_text2iiif.expect(parser)
    def get(cls, vlmid=None):
        """
        endpoint to process and retrieve enriched manifest
        :param vlmid: vlmid for an object that has transcription data
        :return: enriched manifest with alto xml files
        """

        try:
            iiif_manif_url = f"https://www.e-manuscripta.ch/i3f/v20/{vlmid}/manifest"
            r = cls.enrich_manifest_with_alto(iiif_manif_url=iiif_manif_url, vlmid=vlmid)
        except requests.exceptions.JSONDecodeError:
            return f"Manifest '{iiif_manif_url}' could not be loaded / parsed", 500
        if isinstance(r, dict):
            r = json.dumps(r)
        resp = Response(r)
        resp.headers['Access-Control-Allow-Origin'] = '*'
        resp.headers['Content-Type'] = "application/json"
        return resp

    @classmethod
    def enrich_manifest_with_alto(cls, iiif_manif_url, vlmid):
        """
        dd alto xml files with fulltext and coordinates to emanuscripta manifest for each canvas
        :param iiif_manif_url: iiif_manif_url for an object that has transcription data
        :param vlmid: vlmid for an object that has transcription data
        :return: enriched manifest with alto xml files
        """
        params = request.args

        transkribus = IIIFText2Layout(sIIIFManifestUrl=iiif_manif_url,
                                      vlmid = vlmid,
                                          transkribus_collection=TRANSCRIBUS_COLLECTION,
                                          loggingLevel=LOGGING_LEVEL,
                                          login=TRANSCRIBUS_LOGIN,
                                          pwd=TRANSCRIBUS_PWD,
                                      cache=CACHE)

        manifest =deepcopy(transkribus.manifest_data)
        first_page_test = CACHE.get_keyvalue_cache(key=f"{vlmid}_00001.xml")
        #cache_store_date = CACHE.get_keyvalue_cache(key=f"cachetime_{vlmid}_00001.xml") braucht Anpassung im  CacheModule
        #last_change_date = transkribus.last_change_date.get(iiif_manif_url)
        #print(cache_store_date, last_change_date)
        if params.get("only_new_export"):
            transkribus.process_chain(only_new_export=True)
        if params.get("new_processing"):
            transkribus.process_chain()
        if not first_page_test:
            transkribus.process_chain()

        manifest["@id"] = request.base_url
        for n, canvas in enumerate(manifest["sequences"][0]["canvases"], start=1):
            canvas["seeAlso"] =[{"label": "Alto", "type": "Dataset", "@id": f"{request.root_url}/v1/text2iiif/altoxml/{vlmid}/0000{n}.xml",
                                 "format": "application/xml+alto", "profile": "http://www.loc.gov/standards/alto/v4/alto.xsd"}]
            canvas["rendering"] = canvas["seeAlso"]
        r = manifest
        return r

if __name__ == "__main__":
    from flask import Flask, Blueprint
    from flask_restx import Api
    from flask_cors import CORS
    from flask_compress import Compress

    url_prefix = "/v1"
    documentation_endpoint = "/doc/"
    api_v1 = Blueprint('api_v1', __name__, url_prefix=url_prefix)
    home_page = Blueprint('home-page', __name__)

    @home_page.route("/")
    def index_page():
        return 'See <a href="{}{}">Swagger Documentation</a> of this API'.format(url_prefix, documentation_endpoint)

    rdv_api_v1 = Api(api_v1, doc=documentation_endpoint, version="unstable", title='RDV API',
                     description='bu')

    rdv_api_v1.add_namespace(ns_text2iiif)

    rdv_query_app = Flask(__name__)
    rdv_query_app.register_blueprint(api_v1)
    rdv_query_app.register_blueprint(home_page)

    origins = [
        "https?://127.0.0.1.*",
        "https?://localhost.*",
        "https?://ub-.*.unibas.ch",
        "https?://192.168.128.*",
        "https?://.*.ub-digitale-dienste.k8s-001.unibas.ch"
    ]
    CORS(rdv_query_app, origins=origins, supports_credentials=True)
    Compress(rdv_query_app)

    debug = True
    rdv_query_app.run(debug=debug, ssl_context="adhoc", port=5002)
