import json
import logging
import zipfile
import io
import requests
import time

from cache_decorator_redis_ubit import NoCacheDecorator
from TranskribusPyClient.src.TranskribusPyClient.client import TranskribusClient

class TranskribusExtClient(TranskribusClient):
    """
    main objective for overwriting is to change 'Accept` header from XML to JSON
    """

    def _POST(self, sRequest, json={},params={}, data={}, sContentType = "application/xml", sHeaderAccept=None):
        """
        if you set sContentType to None or "", nothing is specified in the request header
        """
        dHeader = {'Cookie':'JSESSIONID=%s'%self._sessionID}
        if sContentType: dHeader['Content-Type'] = sContentType
        if sHeaderAccept:
            dHeader['Accept'] = "application/json, text/plain, */*"
        return requests.post(sRequest, json=json,params=params, headers=dHeader
                             , proxies=self._dProxies, data=data, verify=False)

class IIIFText2Layout():

    def __init__(self, sIIIFManifestUrl, vlmid, transkribus_collection, login, pwd, loggingLevel="ERROR", cache=NoCacheDecorator):
        self.logger = logging.getLogger("IIIFText2Image")
        self.logger.setLevel(loggingLevel)

        self.cache = cache

        self.transkribus_collection = transkribus_collection
        self.sIIIFManifestUrl = sIIIFManifestUrl
        self.vlmid = vlmid
        self.page_ids4emanus_fulltext = []

        self.doer = TranskribusExtClient(loggingLevel="ERROR")
        self.doer.cleanPersistentSession()
        self.doer.auth_login(login, pwd)

        self.processed_manifests, self.last_change_date = self.get_processed_manifests_from_transkribus_collection()

        try:
            self.manifest_data = requests.get(sIIIFManifestUrl).json()
            for n, canvas in enumerate(self.manifest_data["sequences"][0]["canvases"]):
                self.page_ids4emanus_fulltext.append(canvas["@id"].split("/")[-1])
        except requests.exceptions.JSONDecodeError as e:
            raise

    def get_processed_manifests_from_transkribus_collection(self):
        """
        get manifest urls that are already have been processed so same manifest is not reprocessed again
        :return:
        """
        self.doer.sREQ_list_docs= self.doer.sServerUrl + \
                                   f'/rest/collections/{self.transkribus_collection}/list'
        params = self.doer._buidlParamsDic(index=None, nValues=None, sortColumn=None, sortDirection=None)
        myReq = self.doer.sREQ_list_docs
        resp = self.doer._GET(myReq, params=params, accept="application/json")
        looup_manif_id = {doc.get("externalId"): doc.get("docId") for doc in resp.json() if doc.get("externalId")}
        lookup_last_date= {doc.get("externalId"): doc.get("docId") for doc in resp.json() if doc.get("externalId")}
        return looup_manif_id, lookup_last_date

    def create_doc_from_IIIF(self):
        self.doer.sREQ_collection_createDocFromIiifUrl = self.doer.sServerUrl + \
                    f'/rest/collections/{str(self.transkribus_collection)}/createDocFromIiifUrl'
        myReq = self.doer.sREQ_collection_createDocFromIiifUrl
        resp = self.doer._POST(myReq, params={'collId':self.transkribus_collection, "fileName": self.sIIIFManifestUrl },
                          sContentType='application/x-www-form-urlencoded')
        job_id = resp.text
        self.logger.info(f"create_doc_from_IIIF: {job_id}")
        return job_id

    def getpages_fromdoc(self, doc_id):
        self.doer.sREQ_list_pages= self.doer.sServerUrl + \
                                     f'/rest/collections/{self.transkribus_collection}/{doc_id}/pages'
        params = self.doer._buidlParamsDic(index=None, nValues=None, sortColumn=None, sortDirection=None)
        myReq = self.doer.sREQ_list_pages
        resp = self.doer._GET(myReq, params=params, accept="application/json")
        page_ids = []
        for page in resp.json():
            transcript_done = False
            for transkrip in page["tsList"]["transcripts"]:
                if transkrip["status"] == "DONE":
                    transcript_done = True
                    break
            if transcript_done:
                # only excluding pages did not work, because their status will be set to IN_PROGRESS when new layou analysis is started
                return None
            else:
                page_ids.append({"pageId": page.get("pageId")})
        return page_ids

    def startLayoutAnalysis(self, document_id):
        pages = self.getpages_fromdoc(document_id)
        if not pages:
            return None
        description ={"docList":{"docs":[{"docId":str(document_id),"pageList":{"pages": pages}}]}}
        self.doer.sREQ_collection_LA= self.doer.sServerUrl + \
                                                         f'/rest/LA?collId={self.transkribus_collection}' \
                                                         f'&jobImpl=TranskribusLaJob&doBlockSeg=true' \
                                                         f'&doLineSeg=true&doWordSeg=false'
        myReq = self.doer.sREQ_collection_LA
        resp = self.doer._POST(myReq, data=json.dumps(description),sContentType="application/json;charset=utf-8", sHeaderAccept="application/json")
        job_id = json.loads(resp.text)[0]["jobId"]
        return job_id

    def gettextfromemanuscripta(self):
        import html2text
        plain_texts = []
        for page_id in self.page_ids4emanus_fulltext:
            resp = requests.get(f"https://www.e-manuscripta.ch/zuz/download/fulltext/html/{page_id}")
            if resp.ok:
                page_html = resp.text
                plain_text = html2text.html2text(page_html)
                plain_text= plain_text.replace("\n\n", "\n")
                plain_text= plain_text.replace("\n \n", "\n")
                plain_text= plain_text.replace("\n  \n", "\n")
                plain_texts.append(plain_text)
            else:
                plain_texts.append("kein transkribierter Text vorhanden")
        return plain_texts

    def add_text2layout(self, docid, plain_texts, useExistingLayout=True):
        for n, page in enumerate(self.getpages_fromdoc(docid), start=1):
            try:
                plain_text = plain_texts[n-1]
                if plain_text:
                    self.doer.sREQ_collection_assignPlainTextToPage = self.doer.sServerUrl + \
                                        f'/rest/collections/{str(self.transkribus_collection)}/{str(docid)}/{str(n)}/plaintext'
                    myReq = self.doer.sREQ_collection_assignPlainTextToPage
                    resp = self.doer._POST(myReq, params={'useExistingLayout':useExistingLayout, "status": "IN_PROGRESS"},
                                          data=plain_text.encode("utf-8"),
                                          sContentType='text/plain', sHeaderAccept="application/json")
                    ret = resp.text
                    self.logger.info(f"add_text2layout: {ret}")
            except IndexError:
                pass

    def download_altofromTranskribus(self, docid):
        params="""
        {   "commonPars" : {
              "pages" : "1",
              "doExportDocMetadata" : false,
              "doWriteMets" : true,
              "doWriteImages" : false,
              "doExportPageXml" : false,
              "doExportAltoXml" : true,
              "doExportSingleTxtFiles" : false,
              "doWritePdf" : false,
              "doWriteTei" : false,
              "doWriteDocx" : false,
              "doWriteOneTxt" : false,
              "doWriteTagsXlsx" : false,
              "doWriteTagsIob" : false,
              "doWriteTablesXlsx" : false,
              "doWriteStructureInMets" : false,
              "doCreateTitle" : false,
              "useVersionStatus" : "Latest version",
              "writeTextOnWordLevel" : false,
              "doBlackening" : false,
              "selectedTags" : [ "add", "date", "Address", "human_production", "supplied", "work", "unclear", "sic", "structure", "div", "highlight", "place1", "regionType", "speech", "person", "gap", "organization", "comment", "abbrev", "place", "add1", "Initial", "lat" ],
              "font" : "FreeSerif",
              "splitIntoWordsInAltoXml" : true,
              "pageDirName" : "page",
              "fileNamePattern" : "${filename}",
              "useHttps" : true,
              "remoteImgQuality" : "orig",
              "doOverwrite" : true,
              "useOcrMasterDir" : true,
              "exportTranscriptMetadata" : true,
              "updatePageXmlImageDimensions" : false
           }}"""
        export_id = self.doer.exportCollection(self.transkribus_collection, docid, params)
        self.logger.debug(f"export: {export_id}")
        return export_id

    def is_job_done(self, jobid, type=None):
        job_status = self.doer.getJobStatus(jobid)
        if job_status.get("state") in ("FINISHED", "ERROR"):
            if type == "export":
                return job_status.get("result")
            elif type == "create":
                return job_status.get("docId")
            else:
                return True
        else:

            return False

    def wait(self):
        time.sleep(1)

    def extract_alto(self, dl_url):
        r = requests.get(dl_url)
        z = zipfile.ZipFile(io.BytesIO(r.content))
        alto_files = {name: z.read(name) for name in z.namelist() if name.endswith('.xml') and "alto" in name}
        for k, v in alto_files.items():
            k = k.split("/")[-1]
            self.cache.set_keyvalue_cache(key=f"{self.vlmid}_{k}", value=v)
        return alto_files

    def export_and_store_alto(self, doc_id):
        job_id_export = self.download_altofromTranskribus(doc_id)
        while self.is_job_done(job_id_export, type="export") is False:
            self.wait()
        dl_url = self.is_job_done(job_id_export, type="export")
        self.extract_alto(dl_url)
        return True

    def create_layout_add_fulltext(self, doc_id):
        job_id_layout = self.startLayoutAnalysis(document_id=doc_id)
        if job_id_layout is None:
            return None
        while self.is_job_done(job_id_layout) is False:
            self.wait()
        text = self.gettextfromemanuscripta()
        job_id_text2image = self.add_text2layout(doc_id, plain_texts=text)

    def process_chain(self, only_new_export=False):
        doc_id = self.processed_manifests.get(self.sIIIFManifestUrl)

        # reexport Alto from transkribus for existing object
        if only_new_export and doc_id:
            return self.export_and_store_alto(doc_id=doc_id)
        # upload data to transkribus
        elif not self.sIIIFManifestUrl in self.processed_manifests:
            job_id_create = self.create_doc_from_IIIF()
            while self.is_job_done(job_id_create) is False:
                self.wait()
            doc_id = self.is_job_done(job_id_create, type="create")
        else:
            self.logger.info("Manifest already part of collection")

        self.create_layout_add_fulltext(doc_id=doc_id)

        return self.export_and_store_alto(doc_id=doc_id)

if __name__ == "__main__":
    import os
    TRANSCRIBUS_COLLECTION = os.getenv("TRANSCRIBUS_COLLECTION")
    TRANSCRIBUS_LOGIN = os.getenv("TRANSCRIBUS_LOGIN")
    TRANSCRIBUS_PWD = os.getenv("TRANSCRIBUS_PWD")
    LOGGING_LEVEL = "INFO"
    iiif_manif = "https://www.e-manuscripta.ch/i3f/v20/2328191/manifest"
    transkribus = IIIFText2Layout(sIIIFManifestUrl=iiif_manif, transkribus_collection=TRANSCRIBUS_COLLECTION,
                                  loggingLevel=LOGGING_LEVEL, login=TRANSCRIBUS_LOGIN, pwd=TRANSCRIBUS_PWD)

    transkribus.process_chain()
