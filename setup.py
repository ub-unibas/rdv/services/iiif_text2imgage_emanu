#!/usr/bin/python3

import setuptools
with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="iiif_text2image_emanu",
    setup_requires=['setuptools-git-versioning'],
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="enrich emanus manuscripts with Alto data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.switch.ch/ub-unibas/rdv/services/iiif_text2image_emanu",
    install_requires=[
                      'flask', 'flask_cors', 'flask_compress', 'flask-restx',  'markupsafe',
                      'requests',
                      'cache_decorator_redis_ubit', 'gdspreadsheets_ubit',
                      'rdv_data_helpers_ubit', 'html2text'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=setuptools.find_packages(),
    package_data={
        'TranskribusPyClient': ['TranskribusPyClient/*'],
    },
    python_requires=">=3.8",
    include_package_data=True,
    zip_safe=False,
)
